import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExteriorRoutingModule } from './exterior-routing.module';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,ReactiveFormsModule,
    ExteriorRoutingModule
  ]
})
export class ExteriorModule { }
