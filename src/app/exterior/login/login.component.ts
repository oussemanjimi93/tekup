import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user = ["ahmed","oussema","yosri"]
  pass = ["ahmed","oussema","yosri"]
  form:FormGroup
  error:any
  constructor(private router:Router,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    console.log(this.user[0])
    this.initForm()
  }
  initForm(){
    this.form =  this.formBuilder.group({
      pseudo: ['', [Validators.required]],
      password: ['', [Validators.required]],
    })
  }
  handlelogin(){
    this.error = null
    let user = this.form.get("pseudo").value
    let pass = this.form.get("password").value
    let userExist = this.user.indexOf(user)
    console.log(userExist)
    if (userExist != -1){
      if (this.user[userExist] == user && this.pass[userExist] == pass){
        console.log("mawjoud")
        if (user == "oussema"){
          localStorage.setItem('role',"Admin")
          this.router.navigate(["dashboard"])

        }
        if (user == "ahmed"){
          localStorage.setItem('role',"visiteur")
          this.router.navigate(["dashboard"])

        }
        if (user == "yosri"){
          localStorage.setItem('role',"abonne")
          this.router.navigate(["dashboard"])

        }
      }else {
        this.error = "User or Password inccorect"
      }
    }else {
      this.error = "User or Password inccorect"
    }
    // if (this.user.indexOf(user)){
    //   console.log("true")
    // }else {
    //   console.log('false')
    // }
    
  }
}
