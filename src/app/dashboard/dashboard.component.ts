import { Component, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  role: any;
  isVisible = false;
  modalEdit = false;
  addForm: FormGroup
  editForm : FormGroup
  indexEdit :any = null
  listFilm =[ {
    id:0,
    name:"Inception",
    year:2010,
    genre:"science fiction",
    time:"2h",
    disc:"Inception is a 2010 science fiction action written and directed by Christopher Nolan, who also produced the film with Emma Thomas, his wife.",
    img:"https://upload.wikimedia.org/wikipedia/en/2/2e/Inception_%282010%29_theatrical_poster.jpg",
    langue:"eng"


  },
  {
    id:1,
    name:"Mission: Impossible – Fallout",
    year:2018,
    genre:"Action | Adventure",
    time:"2h 28m",
    disc:"Ethan Hunt and his IMF team, along with some familiar allies, race against time after a mission gone wrong.",
    img:"https://upload.wikimedia.org/wikipedia/en/f/ff/MI_%E2%80%93_Fallout.jpg",
    langue:"fr"


  },
  {
    id:2,
    name:"Interstellar",
    year:2014,
    genre:"science fiction",
    time:"2h 20m",
    disc:"Interstellar is a 2014 epic science fiction film co-written, directed and produced by Christopher Nolan. It stars Matthew McConaughey, Anne Hathaway, Jessica Chastain, Bill Irwin, Ellen Burstyn, and Michael Caine. ",
    img:"https://upload.wikimedia.org/wikipedia/en/b/bc/Interstellar_film_poster.jpg",
    langue:"fr"


  },
  {
    id:3,
    name:"Titanic",
    year:1997,
    genre:"Romantic",
    time:"2h 28m",
    disc:"Titanic is a 1997 American epic romance and disaster film directed, written, co-produced, and co-edited by James Cameron. Incorporating both historical and fictionalized aspects.",
    img:"https://upload.wikimedia.org/wikipedia/en/1/19/Titanic_%28Official_Film_Poster%29.png",
    langue:"eng"

  }]

  listFav=[]
  constructor(private formBuilder: FormBuilder,) { }
  ngOnInit(): void {
    this.role = localStorage.getItem("role")
    this.initForm()
    console.log(this.role)


  }

  initForm() {
    this.addForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      years: ['', [Validators.required]],
      genre: ['', [Validators.required]],
      time: ['', [Validators.required]],
      disc: ['', [Validators.required]],
      img:"https://upload.wikimedia.org/wikipedia/en/f/ff/MI_%E2%80%93_Fallout.jpg"

    })
    this.editForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      years: ['', [Validators.required]],
      genre: ['', [Validators.required]],
      time: ['', [Validators.required]],
      disc: ['', [Validators.required]],
      img:"https://upload.wikimedia.org/wikipedia/en/f/ff/MI_%E2%80%93_Fallout.jpg"

    })
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
    console.log(this.addForm.value)
    this.listFilm.push(this.addForm.value)
  }
  handleOkEdit(): void {
    console.log('Button ok clicked!');
    this.modalEdit = false
    if (this.editForm.valid){
      this.confirm(this.indexEdit)
      this.listFilm.splice(this.indexEdit,0,this.editForm.value)
    }
  }
  handleCancel(): void {
    this.isVisible = false;
  }
  handleCancelEdit(): void {
    console.log("azeaze")
    this.modalEdit = false;
  }
  showModal(): void {
    this.isVisible = true;
  }
  cancel(): void {
    //this.nzMessageService.info('click cancel');
  }

  confirm(index): void {
    this.listFilm.splice(index, 1);
  }
  editModal(i){
    this.indexEdit = i
    this.modalEdit = true;
    this.editForm.controls['name'].setValue(this.listFilm[i].name)
    this.editForm.controls['years'].setValue(this.listFilm[i].year)
    this.editForm.controls['genre'].setValue(this.listFilm[i].genre)
    this.editForm.controls['disc'].setValue(this.listFilm[i].disc)
    this.editForm.controls['time'].setValue(this.listFilm[i].time)


  }

  addToFavori(i){
  let id = this.listFav.indexOf(this.listFilm[i])
  console.log(id)
  if (id==-1){
      this.listFav.push(this.listFilm[i])
    }else{
      this.listFav.splice(id,1)
    }
  console.log(this.listFav)
  }

  sortYear(){
    this.listFilm.sort((a,b)=>(a.year - b.year))
      console.log(this.listFilm)

  }
  sortGenre(){
    this.listFilm.sort((a,b) => a.genre.localeCompare(b.genre))
      console.log(this.listFilm)
  }
  sortLang(){
    this.listFilm.sort((a,b) => a.langue.localeCompare(b.langue))
      console.log(this.listFilm)
  }
}
